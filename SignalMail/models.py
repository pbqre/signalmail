from django.db import models
from django.db.models.signals import pre_save, post_save
from django.core.mail import send_mail
from .settings import EMAIL_HOST_USER


class Phone(models.Model):
    name = models.CharField(verbose_name="Phone name", max_length=50)
    details = models.TextField(verbose_name="Phone Details", max_length=1000)
    def __str__(self):
        return self.name

def save_post(sender, instance, **kwargs):
    """
    instance: It's the current Model's object.
    """
    print("After Save...", instance.name)
    send_mail(
        f"Created Phone with name '{instance.name}'", 
        f"Hello, sir. The details of Phone are: \n{instance.details}",
        from_email=EMAIL_HOST_USER,
        recipient_list=[EMAIL_HOST_USER,]
    )


# Connecting save_post function to the post_save signal.
# If you didn't specify sender then it will execute after the .save() method of all Models
post_save.connect(save_post, sender=Phone)