from django.shortcuts import render, HttpResponse
from .forms import PhoneForm



def home(request):
    if request.method == "POST":
        form = PhoneForm(request.POST or None)
        print(form.errors)
        if form.is_valid():
            form.save()
            return HttpResponse("Success")
        return HttpResponse("Error")
    form = PhoneForm()
    return render(request, 'index.html', {'form': form})

